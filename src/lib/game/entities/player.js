ig.module(
    'game.entities.player'
)
.requires(
    'impact.entity'
)
.defines(function() {

    EntityPlayer = ig.Entity.extend({

        size: { x: 12, y: 16 },
        offset: { x: 2, y: 2 },
        health: 100,
        collides: ig.Entity.COLLIDES.ACTIVE,
        type: ig.Entity.TYPE.A,
        checkAgainst: ig.Entity.TYPE.B,

        animSheet: new ig.AnimationSheet('media/human.png', 16, 18),

        init: function (x, y, settings) {

            this.parent(x, y, settings);

            this.addAnim('idleU', 0.6, [1,2,1,0]);
            this.addAnim('idleD', 0.6, [19,20,19,18]);
            this.addAnim('idleL', 0.6, [10,11,10,9]);
            this.addAnim('idleR', 0.6, [28,29,28,27]);

            this.addAnim('walkU', 0.15, [1,2,1,0]);
            this.addAnim('walkD', 0.15, [19,20,19,18]);
            this.addAnim('walkL', 0.15, [28,29,28,27]);
            this.addAnim('walkR', 0.15, [10,11,10,9]);
        },

        update: function() {

            this.parent();
            if(ig.input.pressed('gimli')){
                ig.game.spawnEntity('EntityAxe',this.pos.x,this.pos.y,{direction:this.lastpressed});
            }

            //player movement
            if(ig.input.state('up')){
                this.vel.y = -50;
                this.currentAnim = this.anims.walkU;
                this.lastpressed = 'up';
            }
            else if(ig.input.state('down')) {
                this.vel.y = 50;
                this.currentAnim = this.anims.walkD;
                this.lastpressed = 'down';
            }
            else if(ig.input.state('left')){
                this.vel.x = -50;
                this.currentAnim = this.anims.walkL;
                this.lastpressed = 'left';
            }
            else if(ig.input.state('right')){
                this.vel.x = 50;
                this.currentAnim = this.anims.walkR;
                this.lastpressed = 'right';
            }
            else {
                this.vel.y = 0;
                this.vel.x = 0;
                this.currentAnim = this.anims.idleD;
            }
        }
    });
});
