ig.module(
    'game.entities.axe'
)
.requires(
    'impact.entity'
)
.defines(function() {

    EntityAxe = ig.Entity.extend({

        size: { x: 8, y: 8 },
        vel: { x:100 , y:0 },
        velocity: 100,
        direction: 'right',
        lifetime: 0,
        bounciness: 1,

        collides: ig.Entity.COLLIDES.NONE,
        type: ig.Entity.TYPE.A,
        checkAgainst: ig.Entity.TYPE.B,

        animSheetX: new ig.AnimationSheet('media/axe.png', 16, 16),
        animSheetY: new ig.AnimationSheet('media/axe.png', 16, 16),

        init: function (x, y, settings) {

            this.parent(x, y, settings);

            this.anims.xaxis = new ig.Animation(this.animSheetX,1,[0]);
            this.anims.yaxis = new ig.Animation(this.animSheetY,1,[0]);
            this.currentAnim = this.anims.xaxis;
            if (this.direction == 'right'){
                this.vel.x = this.velocity;
                this.vel.y = 0;
                // this.currentAnim = this.anims.facingRightAnim;
                // this.anims.facingRightAnim.flip.x = false;
            }
            else if (this.direction == 'left'){
                this.vel.x = -this.velocity;
                this.vel.y = 0;
                // this.currentAnim = this.anims.facingLeftAnim;
                // this.anims.facingLeftAnim.flip.x = false;
            }
            else if (this.direction == 'up'){
                this.vel.x = 0;
                this.vel.y = -this.velocity;
                // this.currentAnim = this.anims.facingUpAnim;
                // this.anims.facingUpAnim.flip.x = false;
            }
            else if (this.direction == 'down'){
                this.vel.x = 0;
                this.vel.y = this.velocity;
                // this.currentAnim = this.anims.facingDownAnim;
                // this.anims.facingDownAnim.flip.x = false;
            }
        },
        // put in check() to add behaviors for entity contact
        // other is the implicit value for the entity with which this one makes contact
        check: function(other) {
            // receiveDamage call: other[Entity].willReceiveDamage(atThisValue,fromThis);
            other.receiveDamage(100,this);
            this.kill();
            this.parent();
        },

        update: function() {
            // if it's not 100 old, age it by 1 per frame
            if(this.lifetime<=100){
                this.lifetime +=1;
            }

            // at >100 old, make it expire
            else{
                this.kill();
            }
            this.parent();
        }
    });
});
