ig.module( 
	'game.main' 
)
.requires(
	'impact.game',
	'impact.font',
	'impact.debug.debug',
	'game.levels.level1',
	'game.entities.player',
	'game.entities.enemy',
	'game.entities.axe'
)
.defines(function(){

MyGame = ig.Game.extend({
	
	// Load a font
	font: new ig.Font( 'media/04b03.font.png' ),
	
	
	init: function() {
		// Initialize your game here; bind keys etc.
		this.loadLevel(LevelLevel1);
		ig.input.bind(ig.KEY.UP_ARROW, 'up');
		ig.input.bind(ig.KEY.DOWN_ARROW,'down');
		ig.input.bind(ig.KEY.LEFT_ARROW,'left');
		ig.input.bind(ig.KEY.RIGHT_ARROW,'right');
		ig.input.bind(ig.KEY.SPACE,'gimli');

	},
	
	update: function() {
		// Update all entities and backgroundMaps
		this.parent();
		
		// Add your own, additional update code here, LIKE CAMERAS, THANKS IMPACT BOOK FOR BEING BAD AGAIN

		// set viewport to screen size
		var gameviewport = ig.game.screen;

		// make the main render into the canvas
		var gamecanvas = ig.system;

		// get the first player entity and make it the 'player' variable
		var player = this.getEntitiesByType( EntityPlayer )[0];

		// center the viewport onto the player position
		gameviewport.x = player.pos.x - gamecanvas.width /2;
		gameviewport.y = player.pos.y - gamecanvas.height /2;
	},
	
	draw: function() {
		// Draw all entities and backgroundMaps
		this.parent();
		
		
		// Add your own drawing code here
		var x = ig.system.width/2,
			y = ig.system.height/2;
		
		//draw some text every frame...but let's turn it off for now
		// this.font.draw( 'It Works!', x, y, ig.Font.ALIGN.CENTER );
	}
});


// Start the Game with 60fps, a resolution of 320x240, scaled
// up by a factor of 2
ig.main( '#canvas', MyGame, 60, 320, 240, 2 );

});
